package com.example.bootjdbc.dao;

import com.example.bootjdbc.domain.Employer;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;



@Repository
@Slf4j
public class HibernateEmployerDao implements EmployerDao {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public List<Employer> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try{
        Query query = entityManager.createQuery("from Employer e");
        List<Employer> resultList = query.getResultList();
        return resultList;}finally {
            entityManager.close();
        }
    }

    @Override
    public Employer get(Long id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.find(Employer.class, id);
    }

    @Override
    public void create(Employer employee) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(employee);

            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(Employer employee) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(employee);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Cant update");
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Cant update");
        } finally {
            entityManager.close();
        }

    }
    @Override
 public boolean deleteById(Long id){
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    try {
        entityManager.getTransaction().begin();

        entityManager.remove(entityManager.find(Employer.class,id));
        entityManager.getTransaction().commit();
    } catch (HibernateException ex) {
        log.error("Customer not found");
        entityManager.getTransaction().rollback();
        return false;
    } finally {
        entityManager.close();
        return true;
    }



}

    @Override
    public boolean  delete(Employer employee) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();

            entityManager.remove(entityManager.find(Employer.class,employee.getId()));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Customer not found");
            entityManager.getTransaction().rollback();
        return false;
        } finally {
            entityManager.close();
            return true;
        }

    }
}
