package com.example.bootjdbc.dao;

import com.example.bootjdbc.domain.Account;
import com.example.bootjdbc.domain.Customer;


import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;

@Repository
@Slf4j
public class HibernateCustomerDao implements CrudDao <Customer> {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;



    @Override
    public List<Customer> findAll() {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try{
        Query query = entityManager.createQuery("from Customer c");
        List<Customer> resultList = query.getResultList();
        return resultList;}
        finally {
            entityManager.close();
        }
    }
    @Override
    public Customer getOne(long id) {
        Customer customer = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            customer  = entityManager.find(Customer.class, id);
        } catch (HibernateException ex) {
            log.error("Account not found");
        } finally {
            entityManager.close();
        }
        return customer;
    }
    @Override
    public void save(Customer customer) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
           entityManager.getTransaction().begin();
           entityManager.persist(customer);
          /*  entityManager.createNativeQuery("INSERT INTO customers (customer_id, name,email,age) VALUES (?,?,?,?)")
                    .setParameter(1,customer .getId())
                    .setParameter(2, customer.getName())
                    .setParameter(3, customer .getEmail())
                    .setParameter(4, customer.getAge())
                    .executeUpdate();*/
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean delete(Customer customer) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            Customer customer1 = entityManager.find(Customer.class,customer.getId());
            entityManager.getTransaction().begin();

            entityManager.remove(customer1);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Customer not found");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {

            entityManager.getTransaction().begin();
           customers.forEach(customer ->entityManager.remove(customer));

            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();

        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Customer> customers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {

            entityManager.getTransaction().begin();
            customers .forEach(account ->entityManager.remove(customers));

            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Account not found");
            entityManager.getTransaction().rollback();

        } finally {
            entityManager.close();
        }
    }


    @Override
    public void update(Customer customer) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();

            entityManager.merge(customer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Cant update");
            entityManager.getTransaction().rollback();
            throw new RuntimeException("Cant update");
        } finally {
            entityManager.close();
        }
    }
    @Override
    public boolean deleteById(long id) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
Customer customer =entityManager.find(Customer.class,id);
            entityManager.remove(customer );
            entityManager.getTransaction().commit();
        } catch (HibernateException ex) {
            log.error("Customer not found");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }


}
