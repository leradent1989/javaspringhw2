package com.example.bootjdbc.dao;


import com.example.bootjdbc.domain.Employer;

import java.util.List;

public interface EmployerDao {
    List<Employer> findAll();

    Employer get(Long id);

    void create(Employer employee);

    void update(Employer employee);

    boolean deleteById(Long id);

    boolean delete(Employer employee);
}
