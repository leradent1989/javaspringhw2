package com.example.bootjdbc.resource;

import com.example.bootjdbc.domain.AbstratctEntity;
import com.example.bootjdbc.domain.Account;

import com.example.bootjdbc.domain.Customer;
import com.example.bootjdbc.service.AccountService;

import com.example.bootjdbc.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts")
@CrossOrigin(origins = {"http://localhost:3000"})
public class AcountRestController {

    private final AccountService accountService;
    private final CustomerService customerService;
    @GetMapping

    public List<Account > findAll(){
        return accountService.findAll();


    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long studentId){
   Account account = accountService.getOne(studentId);
        if (account  == null){
            return ResponseEntity.badRequest().body("Account not found");
        }
        return ResponseEntity.ok().body(account );
    }
    @GetMapping("/{id}/customers")
    public ResponseEntity<?> getCustomer(@PathVariable ("id") Long studentId){
        Account account = accountService.getOne(studentId);
        if (account  == null){
            return ResponseEntity.badRequest().body("Account not found");
        }
        return ResponseEntity.ok().body(account.getCustomer() );
    }


    @PostMapping
    public void create(@RequestBody Account account ){

account.setNumber(UUID.randomUUID().toString());
        accountService.save(account );
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Account account ){
        try {


            accountService.update(account) ;
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping

    public ResponseEntity <?> deleteAccount(@RequestBody Account  account){

        try{

            accountService.delete(account);
            return    ResponseEntity.ok().build();

        }catch (RuntimeException e){

            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable ("id") Long studentId){
        try {
            accountService.deleteById(studentId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @PutMapping("/balance")
    public ResponseEntity <?>  addMoney(@RequestParam UUID number, @RequestParam Double  amount ){

        Optional<Account> accountOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny() ;

        if(accountOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        Account editedAccount = accountOptional.get();

        Double accountBalance =  accountOptional.get().getBalance();

        Double accountNewBalance = accountBalance + amount ;

        editedAccount.setBalance(accountNewBalance);

        accountService.update( editedAccount);


        return ResponseEntity.ok().build();

    }

    @PutMapping("/balance/{id}/{amount}")
    public ResponseEntity <?>  replenishAccount(@PathVariable ("id") Long accountId,@PathVariable ("amount") Double amount){


        Account editedAccount = accountService.getOne(accountId);

        Double accountBalance =  accountService.getOne(accountId).getBalance();

        Double accountNewBalance = accountBalance + amount ;

        editedAccount.setBalance(accountNewBalance);

        accountService.update( editedAccount);


        return ResponseEntity.ok().build();

    }


    @DeleteMapping("/balance")
    public ResponseEntity <?>  takeMoney(  @RequestParam  UUID  number, @RequestParam Double  amount ){
        Optional  <Account> accountOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny();
        if(accountOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        Account editedAccount = accountOptional.get();

        Double accountBalance =  accountOptional.get().getBalance();
        if(accountBalance - amount< 0){
            return ResponseEntity.badRequest().body("There is not enough money on your account");
        }

        Double accountNewBalance = accountBalance - amount;

        editedAccount.setBalance(accountNewBalance);
       accountService.update(editedAccount);

        return ResponseEntity.ok().build();

    }

    @PutMapping("/balance/transaction")

    public ResponseEntity <?>  transactionMoney(  @RequestParam  UUID  numberAccountFrom, @RequestParam  UUID  numberAccountTo, @RequestParam Double  amount ){
        Optional  <Account> accountFromOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountFrom.toString())).findAny();
        if(accountFromOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        Optional  <Account> accountToOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountTo.toString())).findAny();
        if(accountToOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        if(accountFromOptional.get().getBalance() -amount <0){

            return ResponseEntity.badRequest().body("Not enough money on account");
        }
        takeMoney(numberAccountFrom ,amount);
        addMoney(numberAccountTo,amount) ;

        return ResponseEntity.ok().build();

    }

}
