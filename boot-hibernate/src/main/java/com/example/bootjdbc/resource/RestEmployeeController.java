package com.example.bootjdbc.resource;

import com.example.bootjdbc.domain.Account;
import com.example.bootjdbc.domain.Customer;
import com.example.bootjdbc.domain.Employer;


import com.example.bootjdbc.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employees")
public class RestEmployeeController {
    private final EmployerService employeeService;

    @GetMapping
    public List<Employer> getAll() {
        return employeeService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?>  getById(@PathVariable("id")  Long  userId) {
       Employer  employee = employeeService.getById(userId );

        if (employee   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(employee  );
    }
    @GetMapping("/customers/{id}")
    public ResponseEntity<?>  getCustomers(@PathVariable("id")  Long  userId) {
        Employer  employee = employeeService.getById(userId );

        if (employee   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(employee.getCustomers() );
    }
    @PostMapping
    public void create(@RequestBody Employer employee ){
        employeeService.create(employee );
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id")Long userId) {
        try {
          employeeService.deleteById(userId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping
    public ResponseEntity<?> deleteEmployee(@RequestBody Employer company) {
        try {
            employeeService.delete(company);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping
    public ResponseEntity <?> update(@RequestBody Employer employee) {
        try {
System.out.println(employee);
            employeeService.update(employee);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

}
