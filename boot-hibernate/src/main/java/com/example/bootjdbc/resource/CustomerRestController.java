package com.example.bootjdbc.resource;

import com.example.bootjdbc.domain.AbstratctEntity;
import com.example.bootjdbc.domain.Account;
import com.example.bootjdbc.domain.Customer;

import com.example.bootjdbc.service.AccountService;
import com.example.bootjdbc.service.CustomerService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
@CrossOrigin( origins = {"http://localhost:3000"})
public class CustomerRestController {

    private final CustomerService customerService;
    private final AccountService accountService ;
    @GetMapping

    public List<Customer > findAll(){
        return customerService.findAll();


    }

    @GetMapping("/{id}/accounts")
    public ResponseEntity<?> getCustomerAccounts(@PathVariable ("id") Long customerId){
        Customer customer = customerService.getOne(customerId);
        List <Account > customerAccounts = customer.getAccounts();
        //  System.out.println(customerAccounts);
        if (customer == null){
            return ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok().body(customerAccounts );
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long customerId){
        Customer customer = customerService.getOne(customerId);
        if (customer  == null){
            return ResponseEntity.badRequest().body("Customer not found");
        }
        System.out.println(customer.getEmployers());
        return ResponseEntity.ok().body(customer );
    }

    @PostMapping
    public void create(@RequestBody Customer customer ){
        customerService.save(customer );
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Customer customer){
        try {

            customerService.update(customer );
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable ("id") Long customerId){
        try {
            customerService.deleteById(customerId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
      @DeleteMapping

    public ResponseEntity <?> deleteCustomer(@RequestBody Customer customer){

        try{

            customerService.delete(customer);
         return    ResponseEntity.ok().build();

        }catch (RuntimeException e){

            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }



    @PutMapping("/account/{id}")

    public ResponseEntity <?> addAccount(@PathVariable Long id,@RequestBody Account account ){

    Customer customer = customerService.getOne(id);

            if(account.getNumber() == ""){

             Account newAccount =   new Account();
             newAccount.setCurrency(account.getCurrency());
             newAccount.setCustomer(customer);
                accountService.save(newAccount);

                List <Account> customerAccounts = customer.getAccounts();
                customerAccounts.add(newAccount) ;
                customer.setAccounts(customerAccounts);

                customerService.update(customer);

            }else{
                 UUID number = UUID.fromString(account.getNumber()) ;
                System.out.println(number);
                Optional<Account> accountOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny() ;
                if(accountOptional.isPresent()){
                    Account accountFromOptional= accountOptional.get();
                Account   account1 =accountService.getOne(  accountFromOptional.getId());
                account1.setCustomer(customer);
           List <Account> customerAccounts = customer.getAccounts();
           customerAccounts.add(account1) ;
           customer.setAccounts(customerAccounts);
           System.out.println(customer.getAccounts());
           customerService.update(customer);

                accountService.update(account1);}else{
                    return ResponseEntity.badRequest().body("Incorrect account number");
                }
            }

            return  ResponseEntity.ok( customer);





    }
    @DeleteMapping("/account/{id}")

    public ResponseEntity <?> deleteAccount(@PathVariable  Long id, @RequestBody Account account ){

        if(accountService.getOne(id) == null){
            return ResponseEntity.badRequest().body("Account does not exist");
        }
        Customer customer = customerService.getOne(id);

        List <Account > customerAccounts = customer.getAccounts();


        if(!customerAccounts.contains(accountService.getOne(account.getId()))){
            return ResponseEntity.badRequest().body("This account isn't in the list ");
        }
        Account account1 =accountService.getOne(account.getId());
        account1.setCustomer(null);
        customerAccounts.remove(accountService.getOne(account.getId()));
        customer.setAccounts(customerAccounts);

        customerService.update(customer);
        accountService.update(account1);
        return ResponseEntity.ok(customer)  ;
    }





}