package com.example.bootjdbc.service;

import com.example.bootjdbc.domain.Customer;

import java.util.List;

public interface CustomerService {
    void save(Customer customer );
    void update(Customer customer);
    boolean delete(Customer customer);
    void deleteAll(List<Customer> customers);
    void saveAll(List<Customer> customers);
    List<Customer> findAll();
    boolean deleteById(long id);
    Customer getOne(Long id);
}
