package com.example.bootjdbc.service;


import com.example.bootjdbc.domain.Employer;

import java.util.List;

public interface EmployerService {
    List<Employer> getAll();
     void create(Employer employee);
    Employer getById(Long userId);
    void deleteById(Long id );
    void update(Employer employee);
    void delete(Employer employee );
}
