package com.example.bootjdbc.domain;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
