package com.example.bootjdbc.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public abstract  class AbstratctEntity <T> {
   @Id

   @Column(name =  "entity_id")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private
   Long id ;




    public Long getId() {
        return id;
    }
}
