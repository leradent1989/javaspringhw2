package com.example.bootjdbc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Setter
@Getter
@Entity
@Table(name="employee")

public class Employer extends AbstratctEntity  {

  /*  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column (name ="employer_id")
    Long id;*/
    @Column(name = "employer_name")
   String name;
 String location;


    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "employee_customer",
            joinColumns = { @JoinColumn(name = "employee_id") },
            inverseJoinColumns = { @JoinColumn(name = "customer_id") })

    List<Customer> customers  = new ArrayList<>();

}
