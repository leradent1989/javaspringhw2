package com.example.bootjdbc;



import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import javax.persistence.EntityManagerFactory;

import javax.persistence.PersistenceUnit;


@SpringBootApplication
@EnableTransactionManagement
@Slf4j
public class BootHibernateApplication implements ApplicationRunner {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;


    public static void main(String[] args) {
        SpringApplication.run(BootHibernateApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        System.out.println("http://localhost:9000/h2-console");
        System.out.println("http://localhost:9000/swagger-ui/index.html");



    }

}
