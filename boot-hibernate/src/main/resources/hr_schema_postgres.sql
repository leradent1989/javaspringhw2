--create tables
BEGIN;

DROP TABLE IF EXISTS customers  CASCADE ;
CREATE TABLE customers (
                                  entity_id  SERIAL PRIMARY KEY,
                               /*   id INTEGER,*/
                                  name VARCHAR(250) NOT NULL,
                                  email  VARCHAR (250) NOT NULL,
                                  age INT NOT NULL



);

DROP TABLE IF EXISTS accounts CASCADE ;
CREATE TABLE accounts (
                                entity_id SERIAL PRIMARY KEY,

                                 number VARCHAR (250) NOT NULL,
                                 currency VARCHAR(250) NOT NULL ,
                                 balance INT NOT NULL ,
                                 customer_id  INTEGER REFERENCES customers (entity_id)


);


DROP TABLE IF EXISTS employee CASCADE ;
CREATE TABLE employee(
                                entity_id SERIAL PRIMARY KEY ,
                                employer_name VARCHAR (250) NOT NULL,
                                location VARCHAR(250) NOT NULL



);
DROP TABLE IF EXISTS employee_customer CASCADE ;
CREATE TABLE employee_customer
(
    id SERIAL PRIMARY KEY ,
    customer_id INT NOT NULL,
    employee_id INT NOT NULL ,
    FOREIGN KEY (customer_id) REFERENCES customers(entity_id),
    FOREIGN KEY (employee_id) REFERENCES employee (entity_id)



);


COMMIT;

