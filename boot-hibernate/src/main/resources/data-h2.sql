



INSERT INTO public.customers ( entity_id,name,email,age) VALUES
        (100,'Alex Smith', 'gtrgtg@gmail.com', 23) ,
        (101,'Cris Thomson', 'feers2@gmail.com', 34),
        (102,'Roger Williams','dtrgrvrg@gmail.com',43),
        (103,'Thomas Spencer','dtrgravrg@gmail.com',35);



INSERT INTO public.accounts (entity_id,number,currency,balance,customer_id) VALUES
                                                          (100,'1a3e8fd2-ab6c-486b-a0c6-4eab6a7dc857','UAH',0,100),
                                                          (101,'d4f441c8-c73a-4de0-a20a-12a58dd24589','EUR', 0,100),
                                                          (102,'78905f8b-e9d7-4e88-a782-d083207bcc20','USD', 0,101),
                                                          (103,'cfcbbf3f-9128-44f8-b51a-ad8813d7093d','UAH',0,101),
                                                          (104,'5dc9a4e3-7323-4cfd-9cb2-5123a03caf2e','EUR', 0,102),
                                                          (105,'e61c4a69-9408-4ad5-9d0c-5996ef52fac5','USD', 0,102),
                                                          (106,'e277a12f-132a-4382-a330-5674657f3e1a','UAH',0,103),
                                                          (107,'50d9f1e8-b4f6-4729-bad1-1e499314d59c','EUR', 0,103);




INSERT INTO public.employee (entity_id,  employer_name, location) VALUES

                                                          (200,'Samsung','LA'),
                                                          (201,'Siemens','NewYork'),
                                                          (202,'Google','Chikago'),
                                                          (203,'Twitter','San Francisco');

INSERT INTO public.employee_customer (id,customer_id,employee_id) VALUES

                                                            (81,100,201),
                                                            (82,101,200),
                                                              (83,102,201),
                                                              (84,103,202),
                                                               (85,101,203),
                                                               (86,103,200);

