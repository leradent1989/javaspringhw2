import React, {Component} from 'react';

import './App.css';
import axios from 'axios';
import Card from './Card';
import CustomerForm from './CustomerForm'
import AccountForm from './AccountForm'
import AddMoneyForm from "./AddMoneyForm";




class App extends Component {
  constructor(props) {
    super(props);
    this.state = {message:[],
    accounts:[]};
  }

async  componentDidMount() {
 const url = 'http://localhost:9000/customers';
    axios.get(url).then(response => {
      console.log('response : ');
      console.log(response);
      this.setState((current) => {

        const newState = { ...current }
        newState.message= response.data
      let  customers = response.data






        return newState
      }
    )}).catch(error => {
      console.log(error);
    });

   // customers.forEach(el =>{


  axios.get(`http://localhost:9000/customers/100/accounts`).then(response => {
        console.log('response : ');
        console.log(response);
        this.setState((current) => {

              const newState = { ...current }

              let customerAccounts = newState.accounts
              // response.data.forEach(el => customerAccounts.push(el))
              customerAccounts.push(response.data)
              newState.accounts = customerAccounts

              return newState
            }
        )}).catch(error => {
        console.log(error);
      });

    axios.get(`http://localhost:9000/customers/101/accounts`).then(response => {
      console.log('response : ');
      console.log(response);
      this.setState((current) => {

            const newState = { ...current }

            let customerAccounts = newState.accounts
            // response.data.forEach(el => customerAccounts.push(el))
            customerAccounts.push(response.data)
            newState.accounts = customerAccounts

            return newState
          }
      )}).catch(error => {
      console.log(error);
    });
  axios.get(`http://localhost:9000/customers/102/accounts`).then(response => {
      console.log('response : ');
      console.log(response);
      this.setState((current) => {

            const newState = { ...current }

            let customerAccounts = newState.accounts
            // response.data.forEach(el => customerAccounts.push(el))
            customerAccounts.push(response.data)
            newState.accounts = customerAccounts

            return newState
          }
      )}).catch(error => {
      console.log(error);
    });

  axios.get(`http://localhost:9000/customers/103/accounts`).then(response => {
      console.log('response : ');
      console.log(response);
      this.setState((current) => {

            const newState = { ...current }

            let customerAccounts = newState.accounts
            // response.data.forEach(el => customerAccounts.push(el))
            customerAccounts.push(response.data)
            newState.accounts = customerAccounts

            return newState
          }
      )}).catch(error => {
      console.log(error);
    });
   // })





}
findAccounts (accounts,customerId){
    return  accounts.filter(account => account.customer.id === customerId);

}


  render() {
    const {message,accounts} = this.state;


    console.log(message)
    console.log(accounts)


    return(
        <>
<div className="App">
<div className="customerForm">
     <h2>Add new customer:</h2>


<CustomerForm  ></CustomerForm></div>
    <div className="accountForm">
    <h2>Add new account to customer:</h2>
    <AccountForm></AccountForm>
</div>
    <div className="moneyForm">
        <h2>Replenish account:</h2>
        <AddMoneyForm></AddMoneyForm>
    </div>
    {message.map(({id,name,email,age}) =><Card key={id}  id ={id} findAccounts ={this.findAccounts}  name= {name} email ={email} age ={age} customers ={message} accounts={accounts} ></Card>)}
  </div>

        </>)
  }
}

export default App;
//disabled={!dirty || !isValid}