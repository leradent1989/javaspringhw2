import React,{useRef} from "react";




const CustomerForm = () => {

    const nameRef = useRef(null);
    const emailRef = useRef(null);
    const ageRef = useRef(null);

    const handleSubmit = (e) => {
        e.preventDefault();

        const body = {
            name: nameRef.current.value,
            email: emailRef.current.value,
            age: ageRef.current.value,
        }

        console.log(body);
        fetch(`http://localhost:9000/customers`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id:null,name:body.name,email:body.email,age:body.age,accounts:[],employee:[] })
        })

    }





    return (
        <form onSubmit={(e) => handleSubmit(e)}>


            <input
                type="text"
                name="name"
                placeholder="Name"
                ref={nameRef}
            /><br/>

            <input
                type="text"
                name="email"
                placeholder="Email"
                ref={emailRef}
            /><br/>

            <input
                type="text"
                name="age"
                placeholder="Age"
                ref={ageRef}
            /><br/>

            <button type="submit">Add new customer</button>
        </form>
    )
}

export default CustomerForm;
/*{


    let initialValues = {

        name: '',
        age:'',
        email:''


    }
    const validationSchema = yup.object().shape({
        name: yup.string()
            .min(3, 'Min 3 symbols')
            .max(100, 'Max 100 symbols')
            .required('Text is required'),
        age: yup.number()
            .min(21, 'Min age is 21')
            .required('Age is required'),
        email: yup.string()
            .email('Email must be correct')
            .required('Email is required'),
    })

    return (

        <Formik
        initialValues={initialValues}

        validationSchema={validationSchema}
         onSubmit={ (values, FormikProps) => {
             console.log(values)
              fetch(`http://localhost:9000/customers`, {
                method: 'POST',
               headers: {
                    'Content-Type': 'application/json'
                },
               body: JSON.stringify({ id:null,name:values.name,email:values.email,age:values.age,accounts:[],employee:[] })
             })


         }

          }

     >
        {({ dirty, isValid }) => {

           return (
               <Form >

                   <Field className="form"
                           type='text'
                           name='name'
                           placeholder='Customer name'

                    />
                 <ErrorMessage name="name">{msg => <span className="error">{msg}</span>}</ErrorMessage>
                 <Field className="form"
                          type='text'
                           name='age'
                           placeholder='Customer age'

                     />
                   <ErrorMessage name="age">{msg => <span className="error">{msg}</span>}</ErrorMessage>
                  <Field className="form"
                         type='text'
                         name='email'
                         placeholder='Customer email'

                  />
                     <ErrorMessage name="email">{msg => <span className="error">{msg}</span>}</ErrorMessage>


                    <button className="form_button" type="submit">Add customer</button>
                 </Form>


             )
        }
        }
     </Formik>
    )
}*/
