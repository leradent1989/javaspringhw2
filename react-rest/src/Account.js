import React, {PureComponent} from 'react'
import './Account.css';


class Account extends PureComponent{

    render(){
        const {id,number,currency,balance,customer} = this.props

        return(
            <>
                    <tr><td>{id}</td><td>{number}</td><td>{currency}</td><td>{balance }</td><td>{customer.name}</td></tr>

            </>
        )
    }
}
export default Account ;