import React, {PureComponent} from 'react'
import Account from './Account';
import "./Card.css"


class Card extends PureComponent{

    render(){
        const {id,name,email,age,accounts,customers } = this.props

let customerIndex = customers.findIndex(el=>el.id === id)

        return(
            <>
<div className= "card">
                <ul>
                    <li><span>Id:</span>{id}</li>
                    <li><span>Name:</span>{name}</li>

                    <li><span>Email:</span>{email}</li>
                    <li><span>Age:</span>{age}</li>
                    <li>Accounts:</li>
                    <table>
                        <thead>
                        <tr><th>Id:</th><th>Number:</th><th>Currency:</th><th>Balance:</th><th>Customer:</th></tr>
                        </thead>
                        <tbody>
                    {

                        accounts[customers.findIndex(el=>el.id === id)]?.map(({id,number,currency,balance}) =><Account key={number }  id ={id}  number= {number} currency ={currency} balance ={balance} customer={customers[customerIndex]} ></Account>)
                        }
                        </tbody>
                    </table>
                </ul>

</div>

            </>
        )
    }
}

export default Card ;